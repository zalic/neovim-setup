vim.g.mapleader = " "

-- in file normal view hitting space -> p -> v takes you to filebrowser ( command :Ex by default ) 
vim.keymap.set("", "<leader>pv", vim.cmd.Ex)

vim.keymap.set("n", "<C-n>", ":n<CR>")
vim.keymap.set("n", "<C-p>", ":N<CR>")
