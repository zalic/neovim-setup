-- package manager
-- https://github.com/wbthomason/packer.nvim
-- :PackerSync to update plugins
-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
	-- Packer can manage itself
	use 'wbthomason/packer.nvim'

	use {
		-- https://github.com/nvim-telescope/telescope.nvim
		'nvim-telescope/telescope.nvim', tag = '0.1.2',
		-- or                            , branch = '0.1.x',
		requires = { {'nvim-lua/plenary.nvim'} }
	}

	use({
		-- https://github.com/chiendo97/intellij.vim
		'chiendo97/intellij.vim',
		as = 'intellij',
		config = function()
			vim.cmd('colorscheme intellij')
		end
	})

	-- https://github.com/nvim-treesitter/nvim-treesitter
	use({
		'nvim-treesitter/nvim-treesitter',
		run = function()
			local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
			ts_update()
	end})
	use('nvim-treesitter/playground')

	-- https://github.com/mbbill/undotree
	use('mbbill/undotree')

	-- https://github.com/tpope/vim-fugitive
	use('tpope/vim-fugitive')

	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'nvim-tree/nvim-web-devicons' }
	}

	-- https://github.com/VonHeikemen/lsp-zero.nvim
	use {
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v1.x',
		requires = {
			-- LSP Support
			{'neovim/nvim-lspconfig'},
			{'williamboman/mason.nvim'},
			{'williamboman/mason-lspconfig.nvim'},

			-- Autocompletion
			{'hrsh7th/nvim-cmp'},
			{'hrsh7th/cmp-buffer'},
			{'hrsh7th/cmp-path'},
			{'saadparwaiz1/cmp_luasnip'},
			{'hrsh7th/cmp-nvim-lsp'},
			{'hrsh7th/cmp-nvim-lua'},

			-- Snippets
			{'L3MON4D3/LuaSnip'},
			{'rafamadriz/friendly-snippets'},
		}
	}


    use {'simrat39/rust-tools.nvim'}

    -- https://github.com/numToStr/Comment.nvim
    use('numToStr/Comment.nvim')

end)
