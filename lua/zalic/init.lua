require("zalic.set")
require("zalic.remap")

-- remove tailing spaces in file before save
-- https://vi.stackexchange.com/a/41388
vim.api.nvim_create_autocmd({ "BufWritePre" }, {
    pattern = {"*"},
    callback = function(ev)
        save_cursor = vim.fn.getpos(".")
        vim.cmd([[%s/\s\+$//e]])
        vim.fn.setpos(".", save_cursor)
    end,
})

-- auto-format go files on save
vim.api.nvim_create_autocmd('BufWritePre', {
  pattern = '*.go',
  callback = function()
    --vim.cmd('LspZeroFormat')
    vim.lsp.buf.format()
  end,
})

function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

local popup
local job

local function close_popup()
    if vim.api.nvim_win_is_valid(popup) then
        vim.fn.jobstop(job)
        vim.api.nvim_win_close(popup, false)
    end
end

-- TODO: should probably add that F5 first saves the file and then executes
local function show_output(data)
    if data then
        -- https://neovim.io/doc/user/api.html#api-floatwin
        -- print(vim.fn.join(data, ", "))

        -- limit float window height to half the screen
        -- local height = math.min(#data, math.floor((vim.api.nvim_win_get_height(0) / 2)))
        local height = math.min(10, math.floor((vim.api.nvim_win_get_height(0) / 2)))

        -- bottom of the screen
        local row = vim.api.nvim_win_get_height(0) - height - 2
        -- middle of the screen
        --local row = (vim.api.nvim_win_get_height(0) / 2) - (height / 2)

        local buf = vim.api.nvim_create_buf(false, true)
        vim.api.nvim_buf_set_lines(buf, 0, -1, true, data)
        local opts = {
            relative = 'editor',
            width = vim.api.nvim_win_get_width(0),
            height = height,
            col = 5,
            row = row,
            anchor = 'NW',
            style = "minimal",
            border = "double"
        }

        popup = vim.api.nvim_open_win(buf, true, opts)

        -- bind to esc key to close the window
        vim.keymap.set("n", "<esc>", function()
            close_popup()
        end)

        return buf
    end
end

local function run_command(command, path)
    if popup and vim.api.nvim_win_is_valid(popup) then
        -- if the popup is open, close it
        close_popup()
        return
    end

    local output = {}
    local buf = show_output(output)
    local function handle_output(chan_id, data, name)
        for k,v in pairs(data) do
            output[#output+1] = v
            vim.api.nvim_buf_set_lines(buf, 0, -1, true, output)
        end
    end
    local function on_exit()
        -- show_output(output)
    end
    -- http://neovim.io/doc/user/builtin.html#jobstart()
    job = vim.fn.jobstart(command, {
        stdout_buffered = false,
        stderr_buffered = false,
        on_exit = on_exit,
        width = vim.api.nvim_win_get_width(0) - 10,
        on_stdout = handle_output,
        on_stderr = handle_output,
        cwd = path
    })
    -- TODO: close job when closing popup window, currently job is left running in the background
end

vim.api.nvim_create_autocmd('FileType', {
    group = vim.api.nvim_create_augroup("run-go", { clear = true }),
    pattern = 'go',
    callback = function()
        vim.keymap.set("n", "<F5>", function()
            -- :help filename-modifiers
            local path = vim.fn.expand('%:p:h')
            local filename = vim.fn.expand('%:t')

            if file_exists(path .. "/go.mod") then
                local command = {"go", "run", "."}
                run_command(command, path)
            else
                local command = {"go", "run", filename}
                run_command(command, path)
            end
        end)
    end
})
vim.api.nvim_create_autocmd('FileType', {
    group = vim.api.nvim_create_augroup("run-java", { clear = true }),
    pattern = 'java',
    callback = function()
        vim.keymap.set("n", "<F5>", function()
            -- :help filename-modifiers
            local path = vim.fn.expand('%:p:h')
            local filename = vim.fn.expand('%:t')
            local command = {"java", filename}
            run_command(command, path)
        end)
    end
})
vim.api.nvim_create_autocmd('FileType', {
    group = vim.api.nvim_create_augroup("run-typescript", { clear = true }),
    pattern = 'typescript',
    callback = function()
        vim.keymap.set("n", "<F5>", function()
            -- :help filename-modifiers
            local path = vim.fn.expand('%:p:h')
            local filename = vim.fn.expand('%:t')
            local command = {"yarn", "ts-node", filename}
            run_command(command, path)
        end)
    end
})
