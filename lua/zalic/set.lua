-- https://vimdoc.sourceforge.net/htmldoc/options.html

-- get that fat juicy cursor
vim.opt.guicursor = ""

-- disable mouse
vim.opt.mouse = ""

-- line numbers
vim.opt.nu = true
-- vim.opt.relativenumber = true

-- number of space characters that will be inserted when the tab key is pressed
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
-- number of space characters inserted for indentation 
vim.opt.shiftwidth = 4
-- use spaces instead of tabs
vim.opt.expandtab = true

vim.opt.smartindent = true

-- in paste mode by default
-- vim.opt.paste = true
-- there is a funny interaction with telescope and paste mode
-- when enabling paste mode after seaching a file in telescope
-- need to <ESC> to jump to files list

vim.opt.wrap = false

-- Use a swapfile for the buffer.
vim.opt.swapfile = true
-- Make a backup before overwriting a file.
-- Leave it around after the file has been successfully written.
vim.opt.backup = true 

vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- When there is a previous search pattern, highlight all its matches.
vim.opt.hlsearch = true
-- While typing a search command, show where the pattern,
-- as it was typed so far, matches.
vim.opt.incsearch = true
-- case insesitive search
vim.opt.ignorecase = true
-- make search case sensitive if search pattern contains upper case characters
vim.opt.smartcase = true

-- Nvim emits true (24-bit) colours in the terminal, if 'termguicolors' is set.
vim.opt.termguicolors = true 

-- https://neovim.io/doc/user/options.html#'scrolloff'
vim.opt.scrolloff = 8

-- https://neovim.io/doc/user/options.html#'signcolumn'
-- always have a spacing for signs on the left side
vim.opt.signcolumn = "yes"

vim.opt.cursorline = true

-- vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "80"

