-- :h comment.plugmap
local comment = require("Comment")
-- https://github.com/numToStr/Comment.nvim/blob/0236521ea582747b58869cb72f70ccfa967d2e89/lua/Comment/api.lua#L70
-- local api = require("Comment.api")
local config = require('Comment.config'):get()

-- https://github.com/numToStr/Comment.nvim#configuration-optional
config.toggler.line = '<F8>'
config.opleader.line = '<F8>'

comment.setup(config);

