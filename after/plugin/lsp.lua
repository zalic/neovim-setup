local lsp = require("lsp-zero")

lsp.preset("recommended")

-- https://github.com/williamboman/mason-lspconfig.nvim/blob/main/doc/server-mapping.md
lsp.ensure_installed({
    'eslint',
    'gopls',
    'phpactor',
    'phpactor',
    'tsserver',
})

local cmp = require('cmp')
local cmp_select = {behavior = cmp.SelectBehavior.Select}
local cmp_mappings = lsp.defaults.cmp_mappings({
    ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
    ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
    ['<C-y>'] = cmp.mapping.confirm({ select = true }),
    ["<C-Space>"] = cmp.mapping.complete(),
})

lsp.on_attach(function(client, bufnr)
    local opts = {buffer = bufnr, remap = false}

    -- jump to definition
    vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
    -- definition in a split window
    vim.keymap.set("n", "gs", function()
        vim.cmd([[
            split
        ]])
        vim.lsp.buf.definition()
    end, opts)
    -- definition in a split window
    vim.keymap.set("n", "gv", function()
        vim.cmd([[
            vsplit
        ]])
        vim.lsp.buf.definition()
    end, opts)
    -- show definition in a floting box
    vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
    -- TODO
    -- vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
    -- vim.keymap.set("n", "<leader>vd", function() vim.diagnostic.open_float() end, opts)
    -- vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
    -- vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
    -- vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
    -- vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.references() end, opts)
    -- vim.keymap.set("n", "<leader>vrn", function() vim.lsp.buf.rename() end, opts)
    -- vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
end)

local rust_lsp = lsp.build_options('rust_analyzer', {})

lsp.setup();

-- Initialize rust_analyzer with rust-tools
require('rust-tools').setup({server = rust_lsp})

vim.diagnostic.config({
    virtual_text = true
})
