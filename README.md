# NeoVim Config

- based on
  - https://www.youtube.com/watch?v=w7i4amO_zaE
  - https://github.com/ThePrimeagen/init.lua/tree/master

## dependencies

- a C compiler (e.g `gcc`)
- `nodejs` and `npm`
- `gopls`
- `composer`
- `rust-analyzer` and `cargo`

## setup


clone repo as
```
~/.config/nvim
```

install packer
```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

open file
```
nvim ~/.config/nvim/lua/zalic/packer.lua
```

read in the config from that file
```
:so
```

install plugins/packages

```
:PackerSync
```

## Keybinds

## FileBrowser view
- `%` - create new file
- `d` - create directory

## Normal view

- `=` - fix tabbing
- `gd` - "Go to definition"
- `gs` - "Go to definition (split window)"
- `gv` - "Go to definition (vsplit window)"
- `K` - to sohw that definition as a floating box
- `CTRL + ^` - go back to previous file
- `<leader>pv` - exec command `:Ex` ( jump to filebrowser view )

## Colorscheme

- https://github.com/chiendo97/intellij.vim

## Plugins

### packer.nvim

- https://github.com/wbthomason/packer.nvim

plugin/package manager

### telescope.nvim

- https://github.com/nvim-telescope/telescope.nvim

a highly extendable fuzzy finder over lists

install [ripgrep](https://github.com/BurntSushi/ripgrep#installation) to make
telescope ignore paths described in `.gitignore`

### nvim-treesitter

- https://github.com/nvim-treesitter/nvim-treesitter

tree-sitter in Neovim to provide functionality like highlighting based on it

### undotree

- https://github.com/mbbill/undotree

Undotree visualizes the undo history and makes it easy to
browse and switch between different undo branches.

### fugitive

- https://github.com/tpope/vim-fugitive

Vim plugin for Git

### LSP Zero

- https://github.com/VonHeikemen/lsp-zero.nvim

Makes [nvim-cmp](https://github.com/hrsh7th/nvim-cmp) (a popular autocompletion plugin) and [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig) work together

### Comment.nvim

- https://github.com/numToStr/Comment.nvim

Smart and Powerful commenting plugin

### lualine

- https://github.com/nvim-lualine/lualine.nvim/

fancy statusline

Note: you need a nerd-font installed in order for the icons to work

Download one from here: https://www.nerdfonts.com/font-downloads

```bash
mkdir -p ~/.local/share/fonts
unzip JetBrainsMono.zip -d ~/.local/share/fonts/
fc-cache ~/.local/share/fonts
```
